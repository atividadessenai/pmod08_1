package br.com.senai.pmod_08_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class TelaPrincipal extends AppCompatActivity implements View.OnClickListener {
    Spinner spinnerCursos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

        spinnerCursos = (Spinner)findViewById(R.id.spinnerCursos);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.cursos, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCursos.setAdapter(adapter);

        Button btnOk = (Button)findViewById(R.id.btnOk);
        btnOk.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        TextView tvStatus = (TextView)findViewById(R.id.tvStatus);
        tvStatus.setText(getResources().getString(R.string.item_selecionado, spinnerCursos.getSelectedItem()));
    }
}
